#!/bin/bash

# 定义自定义存储目录
CUSTOM_STORAGE_DIR="/home/docker/data"

# 设置终端颜色
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
RESET=$(tput sgr0)

# 错误处理函数
function handle_error() {
  echo -e "${RED}[ERROR] $1 ${RESET}" >&2
  read -p "是否要强制卸载Docker？(y/n): " force_uninstall
  if [[ "$force_uninstall" =~ ^[Yy]$ ]]; then
    force_uninstall_docker
  else
    exit 1
  fi
}

# 强制卸载Docker
function force_uninstall_docker() {
  echo -e "${YELLOW}正在强制卸载Docker...${RESET}"
  
  # 强制停止Docker服务
  systemctl stop docker.service || echo -e "${YELLOW}Docker服务未运行。${RESET}"
  systemctl disable docker.service
  pkill -f docker

  # 删除Docker相关的文件和目录
  rm -f /etc/systemd/system/docker.service
  rm -rf /usr/bin/docker*
  rm -rf "$CUSTOM_STORAGE_DIR"
  systemctl daemon-reload
  
  echo -e "${GREEN}Docker已强制卸载。${RESET}"
}

# 用户输入自定义存储目录
if [ -d "$CUSTOM_STORAGE_DIR" ];then
    read -p "输入自定义存储目录 [$CUSTOM_STORAGE_DIR]: " input
    if [ ! -z "$input" ]; then
        CUSTOM_STORAGE_DIR=$input
    fi
fi

# 检查Docker是否已安装
if ! [ -x "$(command -v docker)" ]; then
    echo -e "${YELLOW}Docker未安装或已卸载。${RESET}"
else
  # 检查Docker是否正常运行
  if ! systemctl is-active --quiet docker; then
    handle_error "Docker服务不正常，无法卸载。"
  fi

  # 停止和移除所有容器
  CONTAINERS=$(docker ps -a -q)
  if [ -n "$CONTAINERS" ]; then
    docker stop $CONTAINERS || handle_error "停止Docker容器失败。"
    docker rm $CONTAINERS || handle_error "移除Docker容器失败。"
  fi

  # 移除所有Docker镜像
  IMAGES=$(docker images -q)
  if [ -n "$IMAGES" ]; then
    docker rmi $IMAGES || handle_error "移除Docker镜像失败。"
  fi

  # 判断是否为离线安装
  if [ -x "$(command -v yum)" ]; then
      DOCKER_RESULTS_CENTOS=$(yum list installed | grep "docker" | wc -l)
      if [ "$DOCKER_RESULTS_CENTOS" -ge 1 ];then
          yum remove -y docker-ce docker-ce-cli containerd.io || handle_error "卸载Docker Engine失败。"
      else 
          force_uninstall_docker
      fi
  elif [ -x "$(command -v apt-get)" ]; then
      DOCKER_RESULTS_DEBIAN=$(sudo apt list --installed | grep "docker" | wc -l)
      if [ "$DOCKER_RESULTS_DEBIAN" -ge 1 ];then
          apt-get purge -y docker-ce docker-ce-cli containerd.io || handle_error "卸载Docker Engine失败。"
      else 
          force_uninstall_docker
      fi
  else
      handle_error "不支持的包管理器。"
  fi

  # 删除自定义存储目录
  if [ -d "$CUSTOM_STORAGE_DIR" ]; then
      rm -rf "$CUSTOM_STORAGE_DIR" || handle_error "移除自定义存储目录失败。"
  fi

  echo -e "${GREEN}Docker已成功卸载。${RESET}"
fi

# 卸载Docker Compose
if ! [ -x "$(command -v docker-compose)" ]; then
    echo -e "${YELLOW}Docker Compose未安装或已卸载。${RESET}"
else
    if [ -f "/usr/local/bin/docker-compose" ]; then
        rm /usr/local/bin/docker-compose || handle_error "卸载Docker Compose失败。"
    fi
    echo -e "${GREEN}Docker Compose已成功卸载。${RESET}"
fi
