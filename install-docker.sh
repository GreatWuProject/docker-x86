#!/bin/bash

# 默认参数值
DOCKER_COMPOSE_VERSION="v2.17.2"
DOCKER_DATA_DIR="/home/docker/data"
OFFLINE_MODE=false
DOCKER_VERSION="23.0.6"
DOCKER_INSTALL_OFFLINE="docker-23.0.6.tgz"
OS=""

# 设置红色文字
RED=$(tput setaf 1)
# 设置加粗文字
BOLD=$(tput bold)
# 设置绿色文字
GREEN=$(tput setaf 2)
# 设置黄色文字
YELLOW=$(tput setaf 3)
# 设置蓝色文字
BLUE=$(tput setaf 4)
# 设置彩色文字结束
RESET=$(tput sgr0)

# 错误处理函数
function handle_error() {
  echo -e "${RED}[ERROR] $1 ${RESET}" >&2
  exit 1
}

# 解析命令行参数
while [[ $# -gt 0 ]]; do
  key="$1"
  case $key in
  -d | --data-dir)
    DOCKER_DATA_DIR="$2"
    shift # past argument
    shift # past value
    ;;
  -o | --offline)
    OFFLINE_MODE=true
    shift # past argument
    ;;
  *)
    # unknown option
    handle_error "Unknown option: $1"
    ;;
  esac
done

# 检测操作系统类型
if [ -f /etc/os-release ]; then
  . /etc/os-release
  OS=$ID
  VERSION=$VERSION_ID

  # 检查操作系统类型是否在支持的列表中
  if [ "$OS" != "centos" ] && [ "$OS" != "debian" ] && [ "$OS" != "NFS" ] && [ "$OS" != "kylin" ]; then
    handle_error "This script only supports CentOS, Debian, NFS, and Kylin.  Unsupported OS: $OS."
    exit 1
  fi
else
  handle_error "This script requires the /etc/os-release file to determine the OS."
  exit 1
fi

# 检查当前用户是否为 root
if [ "$(id -u)" -ne 0 ]; then
  handle_error "This script must be run as root."
  exit 1
fi

# 检查是否联网
if [ "$OFFLINE_MODE" = false ]; then
  if ! ping -q -c 1 -W 1 www.baidu.com >/dev/null; then
    handle_error "Please check your network connection."
    exit 1
  fi
fi


# 设置 Docker 数据存储路径
if [ ! -d "$DOCKER_DATA_DIR" ]; then
  mkdir -p "$DOCKER_DATA_DIR" || handle_error "Failed to create directory: $DOCKER_DATA_DIR"
fi

# 安装 Docker
echo -e "${GREEN}Installing Docker...${RESET}"

# 检查是否已经安装 Docker
if [ -x "$(command -v docker)" ]; then
  echo -e "${YELLOW}Docker is already installed.Skipping installing Docker${RESET}"
else
  # 设置 Docker 国内镜像仓库以及默认网络信息
  echo -e "${GREEN}Setting Docker registry...${RESET}"
sudo mkdir -p /etc/docker && sudo bash -c 'cat <<EOF > /etc/docker/daemon.json
{
  "registry-mirrors": ["http://hub-mirror.c.163.com"],
  "data-root": "'"$DOCKER_DATA_DIR"'",
  "bip": "198.20.0.1/24"
}
EOF'

  # 设置 Docker 数据存储目录

 

  if [ "$OFFLINE_MODE" = true ]; then
    tar -xzvf "$DOCKER_INSTALL_OFFLINE" || handle_error "Failed to extract Docker package"
    cp docker/* /usr/bin/
    rm -rf docker
    cp docker.service /etc/systemd/system/
    chmod +x /etc/systemd/system/docker.service

  else
    if [ "$OS" = "centos" ]; then
      yum install -y yum-utils device-mapper-persistent-data lvm2 || handle_error "Failed to install prerequisites"
      yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo || handle_error "Failed to add Docker repository"
      yum install -y docker-ce-${DOCKER_VERSION} docker-ce-cli-${DOCKER_VERSION} containerd.io || handle_error "Failed to install Docker"
    elif [ "$OS" = "debian" ]; then
      apt-get update || handle_error "Failed to update package lists"
      apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common || handle_error "Failed to install prerequisites"
      curl -fsSL https://mirrors.aliyun.com/docker-ce/linux/debian/gpg | sudo apt-key add -
      add-apt-repository "deb [arch=amd64] https://mirrors.aliyun.com/docker-ce/linux/debian/ $(lsb_release -cs) stable" || handle_error "Failed to add Docker repository"
      #curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
      #add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" || handle_error "Failed to add Docker repository"
      apt-get update || handle_error "Failed to update package lists"
      apt-get install -y docker-ce=5:${DOCKER_VERSION}-1~debian.11~bullseye docker-ce-cli=5:${DOCKER_VERSION}-1~debian.11~bullseye containerd.io || handle_error "Failed to install Docker"
    else
      handle_error "Unsupported OS: $OS"
    fi
  fi

  systemctl daemon-reload || handle_error "Failed to reload systemd"
  systemctl start docker.service || handle_error "Failed to start Docker service"
  systemctl enable docker.service || handle_error "Failed to enable Docker service"
  systemctl restart docker.service || handle_error "Failed to enable Docker service"
  echo -e "${GREEN}Docker has been successfully installed and configured!${RESET}"
fi

echo -e "${GREEN}Installing Docker Compose...${RESET}"
# 检查是否已经安装 Docker Compose
if [ -x "$(command -v docker-compose)" ]; then
  echo -e "${YELLOW}Docker Compose is already installed.Skipping installing Docker Compose${RESET}"
else
  if [ ! -x "$(command -v docker-compose)" ]; then
    # 检查是否联网
    if [ "$OFFLINE_MODE" = true ]; then
      cp docker-compose /usr/local/bin/ || handle_error "Failed to install Docker Compose"
    else
      # 安装 Docker Compose
      if ! curl -sSL https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose; then
        handle_error "Failed to download Docker Compose."
      fi
    fi
    chmod +x /usr/local/bin/docker-compose
  fi
  echo -e "${GREEN}Docker Compose have been installed successfully.${RESET}"
fi
